let menu=[
	{name:"Главная", url:"index.html"},
	{name:"О нас", url:"about_us.html"},
	{name:"Новости", url:"news.html"},
	{name:"Контакты", url:"contacts.html"}
];

let headerStartText = `<header align = "center">
<ul>`;
let headerEndText = `</ul>
</header>`;

function setHeader() {
	let headerText = headerStartText;
	menu.forEach(item => {
		headerText += `
		  <li><a href="${item.url}">${item.name}</a></li>`;
	});
  headerText += headerEndText;
  let header =  document.createElement("div");
  header.innerHTML = headerText ;
  document.body.insertAdjacentElement('afterbegin', header );
}

setHeader();

let copyright = "Шитиков Илья";

let footerStartText = `<footer align = "center">&copy;  `;
let footerEndText = `</footer>`;

function setFooter() {
	let footerText =  footerStartText;
	footerText+=copyright;
	footerText+=footerEndText;
	let footer = document.createElement("div");
  footer.innerHTML = footerText ;
  document.body.insertAdjacentElement('beforeend', footer );
}

setFooter();
